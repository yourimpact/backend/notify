package main

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/yourimpact/backend/notify/config"
)

func TestSetupServer(t *testing.T) {
	cfg, err := config.Load("./config.test.yaml")
	assert.NoError(t, err)
	assert.NotNil(t, cfg)

	_, err = setupServer(cfg)
	assert.Error(t, fmt.Errorf("error getting Messaging client: project ID is required to access Firebase Cloud Messaging client"), err)

}
