package main

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/dgrijalva/jwt-go"
	"github.com/go-playground/validator"
	"github.com/google/uuid"
	"github.com/gorilla/mux"
	logger "github.com/tim-koehler/tims-logger"
	"gitlab.com/yourimpact/backend/notify/models"
)

// @Summary Store fcm registration token of caller
// @Description Store fcm registration token of caller
// @Accept json
// @Param reqeust body models.StoreTokenRequest true "Add FCM Registration Token"
// @Success 200 {object} models.SuccessResponse
// @Failure 400 {object} models.ErrorResponse
// @Failure 401 {object} models.ErrorResponse
// @Failure 500 {object} models.ErrorResponse
// @Router /token [post]
// @Security Token
func (server *server) storeToken() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var req models.StoreTokenRequest
		if err := parseAndValidateRequestBody(&req, r, server.validator); err != nil {
			logAndReturnError(w, err, http.StatusBadRequest)
			return
		}

		claims := r.Context().Value("claims").(jwt.MapClaims)
		if claims == nil {
			logAndReturnError(w, fmt.Errorf("no jwt claims in request"), http.StatusBadRequest)
			return
		}

		var rowsAffected int64
		var err error
		if server.repository.IsHardwareIdExisting(req.HardwareId) {
			rowsAffected, err = server.repository.UpdateEntry(req.FcmToken, req.HardwareId)
			if err != nil {
				logAndReturnError(w, err, http.StatusInternalServerError)
				return
			}
		} else {
			rowsAffected, err = server.repository.CreateEntry(
				&models.NotifyDbEntry{
					Id:         uuid.NewString(),
					UserId:     claims["sub"].(string),
					HardwareId: req.HardwareId,
					FcmToken:   req.FcmToken,
				})
			if err != nil {
				logAndReturnError(w, err, http.StatusInternalServerError)
				return
			}
		}
		logAndReturnSuccess(w, fmt.Sprintf("rowsAffected: %d", rowsAffected))
	}
}

// @Summary Retrievs Fcm registration token for given UserId
// @Description Retrievs Fcm registration token for given UserId
// @Success 200 {object} models.TokenResponse
// @Failure 400 {object} models.ErrorResponse
// @Failure 401 {object} models.ErrorResponse
// @Failure 500 {object} models.ErrorResponse
// @Param userId path string true "UserId"
// @Router /token/{userId} [get]
// @Security Token
func (server *server) retrieveToken() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		userId := mux.Vars(r)["userId"]
		if userId == "" {
			logAndReturnError(w, fmt.Errorf("no userId variable in url path"), http.StatusBadRequest)
			return
		}

		entries, err := server.repository.GetEntries(userId)
		if err != nil {
			logAndReturnError(w, err, http.StatusBadRequest)
			return
		}

		var tokenResponse models.TokenResponse
		for _, entry := range entries {
			tokenResponse.UsersTokens = append(tokenResponse.UsersTokens, entry.FcmToken)
		}
		outputJson(w, http.StatusOK, tokenResponse)
	}
}

// @Summary Deletes all Fcm registration tokens for given UserId
// @Description Deletes all Fcm registration tokens for given UserId
// @Success 200 {object} models.SuccessResponse
// @Failure 400 {object} models.ErrorResponse
// @Failure 401 {object} models.ErrorResponse
// @Failure 500 {object} models.ErrorResponse
// @Param userId path string true "UserId"
// @Router /token/{userId} [delete]
// @Security Token
func (server *server) deleteToken() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		userId := mux.Vars(r)["userId"]
		if userId == "" {
			logAndReturnError(w, fmt.Errorf("no userId variable in url path"), http.StatusBadRequest)
			return
		}

		rowsAffected, err := server.repository.DeleteEntry(userId)
		if err != nil {
			logAndReturnError(w, err, http.StatusBadRequest)
			return
		}
		logAndReturnSuccess(w, fmt.Sprintf("rowsAffected: %d", rowsAffected))
	}
}

// @Summary Sends a push notification via firebase fcm to one ore more users
// @Description Sends a push notification via firebase fcm to one ore more users
// @Accept json
// @Param reqeust body models.SendNotificationRequest true "Contains info needed for firebase"
// @Success 200 {object} models.SuccessResponse
// @Failure 400 {object} models.ErrorResponse
// @Failure 401 {object} models.ErrorResponse
// @Failure 500 {object} models.ErrorResponse
// @Router /send [post]
// @Security Token
func (server *server) sendNotification() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var req models.SendNotificationRequest
		if err := parseAndValidateRequestBody(&req, r, server.validator); err != nil {
			logAndReturnError(w, err, http.StatusBadRequest)
			return
		}

		var fcmTokens []string
		for _, userId := range req.ReceiverUserIds {
			entries, err := server.repository.GetEntries(userId)
			if err != nil {
				logAndReturnError(w, err, http.StatusInternalServerError)
				return
			}
			for _, entry := range entries {
				fcmTokens = append(fcmTokens, entry.FcmToken)
			}
		}

		if len(fcmTokens) == 0 {
			logAndReturnError(w, fmt.Errorf("no fcm tokens found for given UserIds"), http.StatusBadRequest)
			return
		}

		var message string
		var err error
		if len(fcmTokens) > 1 {
			message, err = server.fcm.SendMultiNotification(req.Title, req.Message, fcmTokens)
			if err != nil {
				logAndReturnError(w, err, http.StatusBadRequest)
				return
			}
		} else if len(fcmTokens) == 1 {
			message, err = server.fcm.SendSingleNotification(req.Title, req.Message, fcmTokens[0])
			if err != nil {
				logAndReturnError(w, err, http.StatusBadRequest)
				return
			}
		}
		logAndReturnSuccess(w, message)
	}
}

// @Summary Sends a push notification via firebase fcm to all users matching the topic
// @Description Sends a push notification via firebase fcm to all users matching the topic
// @Accept json
// @Param reqeust body models.SendNotificationRequest true "Contains info needed for firebase"
// @Success 200 {object} models.SuccessResponse
// @Failure 400 {object} models.ErrorResponse
// @Failure 401 {object} models.ErrorResponse
// @Failure 500 {object} models.ErrorResponse
// @Router /sendtopic [post]
// @Security Token
func (server *server) sendTopicNotification() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var req models.SendTopicNotificationRequest
		if err := parseAndValidateRequestBody(&req, r, server.validator); err != nil {
			logAndReturnError(w, err, http.StatusBadRequest)
			return
		}

		message, err := server.fcm.SendTopicNotification(req.Title, req.Message, req.Topic)
		if err != nil {
			logAndReturnError(w, err, http.StatusBadRequest)
			return
		}
		logAndReturnSuccess(w, message)
	}
}

func (server *server) handleNoRoute() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		logAndReturnError(w, fmt.Errorf("route not found: /swagger/ for docs"), http.StatusNotFound)
	}
}

func parseAndValidateRequestBody(req interface{}, r *http.Request, validator *validator.Validate) error {
	if err := json.NewDecoder(r.Body).Decode(req); err != nil {
		return err
	}
	if err := validator.Struct(req); err != nil {
		return err
	}
	return nil
}

func logAndReturnError(w http.ResponseWriter, err error, code int) {
	logger.Errorln(err.Error())
	outputJson(w, code, models.ErrorResponse{
		StatusCode: code,
		Message:    err.Error(),
	})
}

func logAndReturnSuccess(w http.ResponseWriter, message string) {
	logger.Debugln(message)
	outputJson(w, http.StatusOK, models.SuccessResponse{
		StatusCode: http.StatusOK,
		Message:    message,
	})
}

func outputJson(w http.ResponseWriter, code int, output interface{}) {
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(code)
	json.NewEncoder(w).Encode(output)
}
