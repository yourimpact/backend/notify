package main

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"strings"

	"github.com/dgrijalva/jwt-go"
	"github.com/lestrrat-go/jwx/jwk"
	logger "github.com/tim-koehler/tims-logger"
)

func (server *server) loggingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		logger.Infoln(fmt.Sprintf("[%s] %s", r.Method, r.RequestURI))
		next.ServeHTTP(w, r)
	})
}

func (server *server) authMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if !server.cfg.EnableJwtAuthorization {
			ctx := context.WithValue(r.Context(), "claims", jwt.MapClaims{"sub": "test-user-id"})
			next.ServeHTTP(w, r.WithContext(ctx))
			return
		}
		rawAccessToken := r.Header.Get("Authorization")
		if rawAccessToken == "" {
			logAndReturnError(w, fmt.Errorf("no 'Authorization' header found"), http.StatusUnauthorized)
			return
		}

		parts := strings.Split(rawAccessToken, " ")
		if len(parts) != 2 {
			logAndReturnError(w, fmt.Errorf("authorization header malformed"), http.StatusUnauthorized)
			return
		}

		token, err := jwt.Parse(parts[1], server.getKey)
		if err != nil {
			logAndReturnError(w, fmt.Errorf("invalid JWT"), http.StatusUnauthorized)
			return
		}

		claims, ok := token.Claims.(jwt.MapClaims)
		if !ok {
			logAndReturnError(w, fmt.Errorf("could net get claims from token"), http.StatusUnauthorized)
			return
		}

		ctx := context.WithValue(r.Context(), "claims", claims)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func (server *server) getKey(token *jwt.Token) (interface{}, error) {

	// TODO: cache response so we don't have to make a request every time we want to verify a JWT
	set, err := jwk.Fetch(context.Background(), server.cfg.AuthJWKsUri)
	if err != nil {
		return nil, err
	}

	keyID, ok := token.Header["kid"].(string)
	if !ok {
		return nil, errors.New("expecting JWT header to have string kid")
	}

	if key, found := set.LookupKeyID(keyID); found {
		var rawkey interface{}
		if err = key.Raw(&rawkey); err != nil {
			return nil, err
		}
		return rawkey, nil
	}

	return nil, fmt.Errorf("unable to find key %q", keyID)
}
