package main

import (
	"bytes"
	"encoding/json"
	"io"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/go-playground/validator"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"gitlab.com/yourimpact/backend/notify/config"
	"gitlab.com/yourimpact/backend/notify/fcm"
	"gitlab.com/yourimpact/backend/notify/models"
	"gitlab.com/yourimpact/backend/notify/repository"
)

func setupTestServer(cfg *config.Config) (*server, error) {
	repo, err := repository.NewPostgres(cfg)
	if err != nil {
		return nil, err
	}

	router := mux.NewRouter().PathPrefix(cfg.RouterPrefix).Subrouter().StrictSlash(true)
	server := &server{
		cfg:        cfg,
		repository: repo,
		router:     router,
		fcm:        &fcm.MockFcm{},
		validator:  validator.New(),
	}

	server.setupRoutes()
	return server, nil
}

func setupDatabase(cfg *config.Config) error {
	repo, err := repository.NewPostgres(cfg)
	if err != nil {
		return err
	}
	repo.Db.MustExec(`CREATE TABLE IF NOT EXISTS "fcm" ("id" uuid NOT NULL, "user_id" varchar NOT NULL, "hardware_id" varchar NOT NULL, "fcm_token" varchar NOT NULL, CONSTRAINT "PK_e7da2209a1f5842712fb68c6ec6" PRIMARY KEY ("id"))`)
	repo.Db.MustExec(`TRUNCATE "fcm"`)
	return nil
}

func teardownDatabase(cfg *config.Config) error {
	repo, err := repository.NewPostgres(cfg)
	if err != nil {
		return err
	}
	repo.Db.MustExec(`TRUNCATE "fcm"`)
	return nil
}

func TestMain(m *testing.M) {
	cfg, err := config.Load("config.test.yaml")
	if err != nil {
		log.Fatalln(err.Error())
	}

	if err := setupDatabase(cfg); err != nil {
		log.Fatalln(err.Error())
	}
	code := m.Run()
	if err := teardownDatabase(cfg); err != nil {
		log.Fatalln(err.Error())
	}
	os.Exit(code)
}

func TestStoreToken(t *testing.T) {
	cfg, err := config.Load("config.test.yaml")
	assert.NoError(t, err)
	assert.NotNil(t, cfg)

	s, err := setupTestServer(cfg)
	assert.NoError(t, err)
	assert.NotNil(t, s)
	defer s.repository.Close()

	requestBody := models.StoreTokenRequest{
		HardwareId: "hdw_id",
		FcmToken:   "xyfcm-tokenxy",
	}

	requestBodyData, err := json.Marshal(requestBody)
	assert.NoError(t, err)

	t.Run("Testing with auth middleware but without token", func(t *testing.T) {
		req, err := http.NewRequest("POST", "/notify/token", bytes.NewBuffer(requestBodyData))
		assert.NoError(t, err)
		assert.NotNil(t, req)

		rr := httptest.NewRecorder()
		s.router.ServeHTTP(rr, req)

		data, err := io.ReadAll(rr.Body)
		assert.NoError(t, err)
		assert.EqualValues(t, "{\"statusCode\":401,\"message\":\"no 'Authorization' header found\"}\n", string(data))
		assert.EqualValues(t, 401, rr.Code)
	})

	t.Run("Testing with auth middleware but with wrong token", func(t *testing.T) {
		req, err := http.NewRequest("POST", "/notify/token", bytes.NewBuffer(requestBodyData))
		req.Header.Add("Authorization", "Bearer wrong_token")
		assert.NoError(t, err)
		assert.NotNil(t, req)

		rr := httptest.NewRecorder()
		s.router.ServeHTTP(rr, req)

		data, err := io.ReadAll(rr.Body)
		assert.NoError(t, err)
		assert.EqualValues(t, "{\"statusCode\":401,\"message\":\"invalid JWT\"}\n", string(data))
		assert.EqualValues(t, 401, rr.Code)
	})

	cfg.EnableJwtAuthorization = false
	s, err = setupTestServer(cfg)
	assert.NoError(t, err)
	assert.NotNil(t, s)

	t.Run("Testing with auth middleware bypass", func(t *testing.T) {
		req, err := http.NewRequest("POST", "/notify/token", bytes.NewBuffer(requestBodyData))
		assert.NoError(t, err)
		assert.NotNil(t, req)

		rr := httptest.NewRecorder()
		s.router.ServeHTTP(rr, req)

		data, err := io.ReadAll(rr.Body)
		assert.NoError(t, err)
		assert.EqualValues(t, "{\"statusCode\":200,\"message\":\"rowsAffected: 1\"}\n", string(data))
		assert.EqualValues(t, 200, rr.Code)
	})

	t.Run("Testing with auth middleware bypass again to update fcm token", func(t *testing.T) {
		requestBody = models.StoreTokenRequest{
			HardwareId: "hdw_id",
			FcmToken:   "xyfcm-tokenfoobarxy",
		}
		requestBodyData, err := json.Marshal(requestBody)
		assert.NoError(t, err)

		req, err := http.NewRequest("POST", "/notify/token", bytes.NewBuffer(requestBodyData))
		assert.NoError(t, err)
		assert.NotNil(t, req)

		rr := httptest.NewRecorder()
		s.router.ServeHTTP(rr, req)

		data, err := io.ReadAll(rr.Body)
		assert.NoError(t, err)
		assert.EqualValues(t, "{\"statusCode\":200,\"message\":\"rowsAffected: 1\"}\n", string(data))
		assert.EqualValues(t, 200, rr.Code)
	})
}

func TestRetrieveToken(t *testing.T) {
	cfg, err := config.Load("config.test.yaml")
	assert.NoError(t, err)
	assert.NotNil(t, cfg)

	s, err := setupTestServer(cfg)
	assert.NoError(t, err)
	assert.NotNil(t, s)
	defer s.repository.Close()

	t.Run("Testing with auth middleware but without token", func(t *testing.T) {
		req, err := http.NewRequest("GET", "/notify/test-user-id", nil)
		assert.NoError(t, err)
		assert.NotNil(t, req)

		rr := httptest.NewRecorder()
		s.router.ServeHTTP(rr, req)

		data, err := io.ReadAll(rr.Body)
		assert.NoError(t, err)
		assert.EqualValues(t, "{\"statusCode\":401,\"message\":\"no 'Authorization' header found\"}\n", string(data))
		assert.EqualValues(t, 401, rr.Code)
	})

	t.Run("Testing with auth middleware but with wrong token", func(t *testing.T) {
		req, err := http.NewRequest("GET", "/notify/test-user-id", nil)
		req.Header.Add("Authorization", "Bearer wrong_token")
		assert.NoError(t, err)
		assert.NotNil(t, req)

		rr := httptest.NewRecorder()
		s.router.ServeHTTP(rr, req)

		data, err := io.ReadAll(rr.Body)
		assert.NoError(t, err)
		assert.EqualValues(t, "{\"statusCode\":401,\"message\":\"invalid JWT\"}\n", string(data))
		assert.EqualValues(t, 401, rr.Code)
	})

	cfg.EnableJwtAuthorization = false
	s, err = setupTestServer(cfg)
	assert.NoError(t, err)
	assert.NotNil(t, s)

	t.Run("Testing with auth middleware bypass", func(t *testing.T) {
		req, err := http.NewRequest("GET", "/notify/token/test-user-id", nil)
		assert.NoError(t, err)
		assert.NotNil(t, req)

		rr := httptest.NewRecorder()
		s.router.ServeHTTP(rr, req)

		data, err := io.ReadAll(rr.Body)
		assert.NoError(t, err)
		assert.EqualValues(t, "{\"usersTokens\":[\"xyfcm-tokenfoobarxy\"]}\n", string(data))
		assert.EqualValues(t, 200, rr.Code)
	})

	t.Run("Testing with auth middleware bypass but wrong user-id", func(t *testing.T) {
		req, err := http.NewRequest("GET", "/notify/token/foobarbaz", nil)
		assert.NoError(t, err)
		assert.NotNil(t, req)

		rr := httptest.NewRecorder()
		s.router.ServeHTTP(rr, req)

		data, err := io.ReadAll(rr.Body)
		assert.NoError(t, err)
		assert.EqualValues(t, "{\"usersTokens\":null}\n", string(data))
		assert.EqualValues(t, 200, rr.Code)
	})
}

func TestSendNotification(t *testing.T) {
	cfg, err := config.Load("config.test.yaml")
	assert.NoError(t, err)
	assert.NotNil(t, cfg)

	s, err := setupTestServer(cfg)
	assert.NoError(t, err)
	assert.NotNil(t, s)
	defer s.repository.Close()

	t.Run("Testing send single Notification with auth middleware", func(t *testing.T) {
		requestBody := models.SendNotificationRequest{
			ReceiverUserIds: []string{"user1-id"},
			Title:           "Test-Title",
			Message:         "Test-Message",
		}

		requestBodyData, err := json.Marshal(requestBody)
		assert.NoError(t, err)

		req, err := http.NewRequest("POST", "/notify/send", bytes.NewBuffer(requestBodyData))
		assert.NoError(t, err)
		assert.NotNil(t, req)

		rr := httptest.NewRecorder()
		s.router.ServeHTTP(rr, req)

		data, err := io.ReadAll(rr.Body)
		assert.NoError(t, err)
		assert.EqualValues(t, "{\"statusCode\":401,\"message\":\"no 'Authorization' header found\"}\n", string(data))
		assert.EqualValues(t, 401, rr.Code)
	})

	t.Run("Testing send topic Notification with auth middleware", func(t *testing.T) {
		requestBody := models.SendTopicNotificationRequest{
			Topic:   "Test-Topic",
			Title:   "Test-Title",
			Message: "Test-Message",
		}

		requestBodyData, err := json.Marshal(requestBody)
		assert.NoError(t, err)

		req, err := http.NewRequest("POST", "/notify/sendtopic", bytes.NewBuffer(requestBodyData))
		assert.NoError(t, err)
		assert.NotNil(t, req)

		rr := httptest.NewRecorder()
		s.router.ServeHTTP(rr, req)

		data, err := io.ReadAll(rr.Body)
		assert.NoError(t, err)
		assert.EqualValues(t, "{\"statusCode\":401,\"message\":\"no 'Authorization' header found\"}\n", string(data))
		assert.EqualValues(t, 401, rr.Code)
	})

	cfg.EnableJwtAuthorization = false
	s, err = setupTestServer(cfg)
	assert.NoError(t, err)
	assert.NotNil(t, s)

	t.Run("Testing send single Notification wrong user id", func(t *testing.T) {
		requestBody := models.SendNotificationRequest{
			ReceiverUserIds: []string{"user1-id"},
			Title:           "Test-Title",
			Message:         "Test-Message",
		}

		requestBodyData, err := json.Marshal(requestBody)
		assert.NoError(t, err)

		req, err := http.NewRequest("POST", "/notify/send", bytes.NewBuffer(requestBodyData))
		assert.NoError(t, err)
		assert.NotNil(t, req)

		rr := httptest.NewRecorder()
		s.router.ServeHTTP(rr, req)

		data, err := io.ReadAll(rr.Body)
		assert.NoError(t, err)
		assert.EqualValues(t, "{\"statusCode\":400,\"message\":\"no fcm tokens found for given UserIds\"}\n", string(data))
		assert.EqualValues(t, 400, rr.Code)
	})

	t.Run("Testing send single Notification correct user id", func(t *testing.T) {
		requestBody := models.SendNotificationRequest{
			ReceiverUserIds: []string{"test-user-id"},
			Title:           "Test-Title",
			Message:         "Test-Message",
		}

		requestBodyData, err := json.Marshal(requestBody)
		assert.NoError(t, err)

		req, err := http.NewRequest("POST", "/notify/send", bytes.NewBuffer(requestBodyData))
		assert.NoError(t, err)
		assert.NotNil(t, req)

		rr := httptest.NewRecorder()
		s.router.ServeHTTP(rr, req)

		data, err := io.ReadAll(rr.Body)
		assert.NoError(t, err)
		assert.EqualValues(t, "{\"statusCode\":200,\"message\":\"Success\"}\n", string(data))
		assert.EqualValues(t, 200, rr.Code)
	})

	t.Run("Testing send multi Notification", func(t *testing.T) {
		requestBody := models.SendNotificationRequest{
			ReceiverUserIds: []string{"test-user-id", "user2-id"},
			Title:           "Test-Title",
			Message:         "Test-Message",
		}

		requestBodyData, err := json.Marshal(requestBody)
		assert.NoError(t, err)

		req, err := http.NewRequest("POST", "/notify/send", bytes.NewBuffer(requestBodyData))
		assert.NoError(t, err)
		assert.NotNil(t, req)

		rr := httptest.NewRecorder()
		s.router.ServeHTTP(rr, req)

		data, err := io.ReadAll(rr.Body)
		assert.NoError(t, err)
		assert.EqualValues(t, "{\"statusCode\":200,\"message\":\"Success\"}\n", string(data))
		assert.EqualValues(t, 200, rr.Code)
	})
	t.Run("Testing send multi Notification valid userIds", func(t *testing.T) {
		requestBody := models.SendNotificationRequest{
			ReceiverUserIds: []string{"test-user-id", "test-user-id"},
			Title:           "Test-Title",
			Message:         "Test-Message",
		}

		requestBodyData, err := json.Marshal(requestBody)
		assert.NoError(t, err)

		req, err := http.NewRequest("POST", "/notify/send", bytes.NewBuffer(requestBodyData))
		assert.NoError(t, err)
		assert.NotNil(t, req)

		rr := httptest.NewRecorder()
		s.router.ServeHTTP(rr, req)

		data, err := io.ReadAll(rr.Body)
		assert.NoError(t, err)
		assert.EqualValues(t, "{\"statusCode\":200,\"message\":\"Success\"}\n", string(data))
		assert.EqualValues(t, 200, rr.Code)
	})

	t.Run("Testing send topic Notification", func(t *testing.T) {
		requestBody := models.SendTopicNotificationRequest{
			Topic:   "Test-Topic",
			Title:   "Test-Title",
			Message: "Test-Message",
		}

		requestBodyData, err := json.Marshal(requestBody)
		assert.NoError(t, err)

		req, err := http.NewRequest("POST", "/notify/sendtopic", bytes.NewBuffer(requestBodyData))
		assert.NoError(t, err)
		assert.NotNil(t, req)

		rr := httptest.NewRecorder()
		s.router.ServeHTTP(rr, req)

		data, err := io.ReadAll(rr.Body)
		assert.NoError(t, err)
		assert.EqualValues(t, "{\"statusCode\":200,\"message\":\"Success\"}\n", string(data))
		assert.EqualValues(t, 200, rr.Code)
	})
}

func TestDeleteToken(t *testing.T) {
	cfg, err := config.Load("config.test.yaml")
	assert.NoError(t, err)
	assert.NotNil(t, cfg)

	s, err := setupTestServer(cfg)
	assert.NoError(t, err)
	assert.NotNil(t, s)
	defer s.repository.Close()

	t.Run("Testing with auth middleware but without token", func(t *testing.T) {
		req, err := http.NewRequest("DELETE", "/notify/test-user-id", nil)
		assert.NoError(t, err)
		assert.NotNil(t, req)

		rr := httptest.NewRecorder()
		s.router.ServeHTTP(rr, req)

		data, err := io.ReadAll(rr.Body)
		assert.NoError(t, err)
		assert.EqualValues(t, "{\"statusCode\":401,\"message\":\"no 'Authorization' header found\"}\n", string(data))
		assert.EqualValues(t, 401, rr.Code)
	})

	t.Run("Testing with auth middleware but with wrong token", func(t *testing.T) {
		req, err := http.NewRequest("DELETE", "/notify/test-user-id", nil)
		req.Header.Add("Authorization", "Bearer wrong_token")
		assert.NoError(t, err)
		assert.NotNil(t, req)

		rr := httptest.NewRecorder()
		s.router.ServeHTTP(rr, req)

		data, err := io.ReadAll(rr.Body)
		assert.NoError(t, err)
		assert.EqualValues(t, "{\"statusCode\":401,\"message\":\"invalid JWT\"}\n", string(data))
		assert.EqualValues(t, 401, rr.Code)
	})

	cfg.EnableJwtAuthorization = false
	s, err = setupTestServer(cfg)
	assert.NoError(t, err)
	assert.NotNil(t, s)

	t.Run("Testing with auth middleware bypass", func(t *testing.T) {
		req, err := http.NewRequest("DELETE", "/notify/token/test-user-id", nil)
		assert.NoError(t, err)
		assert.NotNil(t, req)

		rr := httptest.NewRecorder()
		s.router.ServeHTTP(rr, req)

		data, err := io.ReadAll(rr.Body)
		assert.NoError(t, err)
		assert.EqualValues(t, "{\"statusCode\":200,\"message\":\"rowsAffected: 1\"}\n", string(data))
		assert.EqualValues(t, 200, rr.Code)
	})

	t.Run("Testing with auth middleware bypass but table db now", func(t *testing.T) {
		req, err := http.NewRequest("DELETE", "/notify/token/test-user-id", nil)
		assert.NoError(t, err)
		assert.NotNil(t, req)

		rr := httptest.NewRecorder()
		s.router.ServeHTTP(rr, req)

		data, err := io.ReadAll(rr.Body)
		assert.NoError(t, err)
		assert.EqualValues(t, "{\"statusCode\":200,\"message\":\"rowsAffected: 0\"}\n", string(data))
		assert.EqualValues(t, 200, rr.Code)
	})
}

func TestNoRoute(t *testing.T) {
	cfg, err := config.Load("config.test.yaml")
	assert.NoError(t, err)
	assert.NotNil(t, cfg)

	cfg.EnableJwtAuthorization = false

	s, err := setupTestServer(cfg)
	assert.NoError(t, err)
	assert.NotNil(t, s)
	defer s.repository.Close()

	req, err := http.NewRequest("GET", "/notify/foobarbaz", nil)
	assert.NoError(t, err)
	assert.NotNil(t, req)

	rr := httptest.NewRecorder()
	s.router.ServeHTTP(rr, req)

	data, err := io.ReadAll(rr.Body)
	assert.NoError(t, err)
	assert.EqualValues(t, "{\"statusCode\":404,\"message\":\"route not found: /swagger/ for docs\"}\n", string(data))
	assert.EqualValues(t, 404, rr.Code)
}
