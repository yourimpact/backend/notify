package main

import (
	"net/http"

	"github.com/rs/cors"
	httpSwagger "github.com/swaggo/http-swagger"
	_ "gitlab.com/yourimpact/backend/notify/docs"
)

func (server *server) setupRoutes() {
	cors := cors.New(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowedMethods:   []string{"GET", "HEAD", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowCredentials: true,
	})

	server.router.PathPrefix("/swagger/").Handler(httpSwagger.WrapHandler)
	server.router.Use(cors.Handler)
	server.router.Use(server.loggingMiddleware)

	apirouter := server.router.NewRoute().Subrouter()
	apirouter.HandleFunc("/token", server.storeToken()).Methods(http.MethodPost, http.MethodOptions)
	apirouter.HandleFunc("/token/{userId}", server.retrieveToken()).Methods(http.MethodGet, http.MethodOptions)
	apirouter.HandleFunc("/token/{userId}", server.deleteToken()).Methods(http.MethodDelete, http.MethodOptions)
	apirouter.HandleFunc("/send", server.sendNotification()).Methods(http.MethodPost, http.MethodOptions)
	apirouter.HandleFunc("/sendtopic", server.sendTopicNotification()).Methods(http.MethodPost, http.MethodOptions)
	apirouter.PathPrefix("/").Handler(server.handleNoRoute())
	apirouter.Use(server.authMiddleware)
}
