package docs

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestDocs(t *testing.T) {
	s := s{}
	t.Run("Test docs", func(t *testing.T) {
		assert.NotEqualValues(t, "", s.ReadDoc())
	})
}
