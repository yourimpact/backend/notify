package models

type StoreTokenRequest struct {
	HardwareId string `json:"hardwareId" validate:"required"`
	FcmToken   string `json:"fcmToken" validate:"required"`
}

type SendNotificationRequest struct {
	ReceiverUserIds []string `json:"receiverUserIds" validate:"required"`
	Title           string   `json:"title" validate:"required"`
	Message         string   `json:"message" validate:"required"`
}

type SendTopicNotificationRequest struct {
	Topic   string `json:"topic" validate:"required"`
	Title   string `json:"title" validate:"required"`
	Message string `json:"message" validate:"required"`
}

type TokenResponse struct {
	UsersTokens []string `json:"usersTokens"`
}
type ErrorResponse struct {
	StatusCode int    `json:"statusCode"`
	Message    string `json:"message"`
}

type SuccessResponse struct {
	StatusCode int    `json:"statusCode"`
	Message    string `json:"message"`
}
