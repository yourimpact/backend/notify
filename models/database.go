package models

type NotifyDbEntry struct {
	Id         string `json:"Id" db:"id"`
	UserId     string `json:"UserId" db:"user_id"`
	HardwareId string `json:"HardwareId" db:"hardware_id"`
	FcmToken   string `json:"FcmToken" db:"fcm_token"`
}
