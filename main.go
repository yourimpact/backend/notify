package main

import (
	"log"
	"net/http"

	"github.com/go-playground/validator"
	"github.com/gorilla/mux"
	logger "github.com/tim-koehler/tims-logger"
	"gitlab.com/yourimpact/backend/notify/config"
	"gitlab.com/yourimpact/backend/notify/docs"
	"gitlab.com/yourimpact/backend/notify/fcm"
	"gitlab.com/yourimpact/backend/notify/repository"
)

type server struct {
	cfg        *config.Config
	repository repository.Repository
	router     *mux.Router
	fcm        fcm.Fcm
	validator  *validator.Validate
}

// @title Swagger notify API
// @version 1.0
// @description This microservice handles all about notifications. From storing, updating and deleting fcm registration tokens to sending notifications for specific clients, a group of clients or whole topics.
// @termsOfService http://swagger.io/terms/

// @contact.name API Support
// @contact.url http://www.swagger.io/support
// @contact.email support@swagger.io

// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html
// @securityDefinitions.apikey Token
// @in header
// @name Authorization
func main() {
	cfg, err := config.Load("./config.yaml")
	if err != nil {
		log.Fatalln(err.Error())
	}

	server, err := setupServer(cfg)
	if err != nil {
		log.Fatalln(err.Error())
	}
	defer server.repository.Close()

	logger.Infoln("Running on [:8080]...")
	http.ListenAndServe(":8080", server.router)
}

func setupServer(cfg *config.Config) (*server, error) {
	logger.SetLogLevel(cfg.LogLevel)
	logger.SetLogType(cfg.LogType)

	docs.SwaggerInfo.Host = cfg.SwaggerHost
	docs.SwaggerInfo.BasePath = cfg.RouterPrefix

	repo, err := repository.NewPostgres(cfg)
	if err != nil {
		return nil, err
	}

	fcm, err := fcm.New(cfg)
	if err != nil {
		return nil, err
	}

	router := mux.NewRouter().PathPrefix(cfg.RouterPrefix).Subrouter().StrictSlash(true)
	server := &server{
		cfg:        cfg,
		repository: repo,
		router:     router,
		fcm:        fcm,
		validator:  validator.New(),
	}

	server.setupRoutes()

	return server, nil
}
