package repository

import "gitlab.com/yourimpact/backend/notify/models"

type Repository interface {
	CreateEntry(entry *models.NotifyDbEntry) (int64, error)
	GetEntries(userId string) ([]models.NotifyDbEntry, error)
	UpdateEntry(fcmToken, hardwareId string) (int64, error)
	DeleteEntry(id string) (int64, error)
	IsHardwareIdExisting(hardwareId string) bool
	Close() error
}
