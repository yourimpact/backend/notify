package repository

import (
	"fmt"

	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"gitlab.com/yourimpact/backend/notify/config"
	"gitlab.com/yourimpact/backend/notify/models"
)

type Postgres struct {
	Db *sqlx.DB
}

func NewPostgres(cfg *config.Config) (*Postgres, error) {
	db, err := sqlx.Connect("postgres",
		fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
			cfg.Database.Host, cfg.Database.Port, cfg.Database.User, cfg.Database.Password, cfg.Database.DbName))
	if err != nil {
		return nil, err
	}

	if err := db.Ping(); err != nil {
		return nil, err
	}

	return &Postgres{
		Db: db,
	}, nil
}

func (postgres *Postgres) CreateEntry(entry *models.NotifyDbEntry) (int64, error) {
	entry.Id = uuid.NewString()
	result, err := postgres.Db.NamedExec(`
			INSERT INTO fcm (id, user_id, hardware_id, fcm_token)
			VALUES (:id, :user_id, :hardware_id, :fcm_token)`, entry)
	if err != nil {
		return 0, err
	}
	return result.RowsAffected()
}

func (postgres *Postgres) GetEntries(userId string) ([]models.NotifyDbEntry, error) {
	var entries []models.NotifyDbEntry
	if err := postgres.Db.Select(&entries, "SELECT * FROM fcm WHERE user_id = $1", userId); err != nil {
		return nil, err
	}
	return entries, nil
}

func (postgres *Postgres) UpdateEntry(fcmToken, hardwareId string) (int64, error) {
	result, err := postgres.Db.Exec(`
			UPDATE fcm
			SET fcm_token = $1
			WHERE hardware_id = $2`, fcmToken, hardwareId)
	if err != nil {
		return 0, err
	}
	return result.RowsAffected()
}

func (postgres *Postgres) DeleteEntry(id string) (int64, error) {
	result, err := postgres.Db.Exec("DELETE FROM fcm WHERE user_id = $1", id)
	if err != nil {
		return 0, err
	}
	return result.RowsAffected()
}

func (postgres *Postgres) IsHardwareIdExisting(hardwareId string) bool {
	row := postgres.Db.QueryRowx(`
	SELECT count(id)
	FROM public.fcm
	WHERE hardware_id = $1`, hardwareId)

	var count int
	row.Scan(&count)

	return count > 0
}

func (postgres *Postgres) Close() error {
	return postgres.Db.Close()
}
