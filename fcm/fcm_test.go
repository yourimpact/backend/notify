package fcm

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/yourimpact/backend/notify/config"
)

func TestFcm(t *testing.T) {
	cfg, err := config.Load("../config.template.yaml")
	assert.NoError(t, err)
	assert.NotNil(t, cfg)

	fcm, err := New(cfg)
	assert.Error(t, err)
	assert.Nil(t, fcm)

	//output, err := fcm.SendSingleNotification("Title", "Message", "Token")
	//assert.Error(t, err)
	//assert.EqualValues(t, output, "")
}
