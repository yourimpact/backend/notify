package fcm

type MockFcm struct {
}

func NewMock() *MockFcm {
	return &MockFcm{}
}

func (fcm *MockFcm) SendSingleNotification(title, message, fcmToken string) (string, error) {
	return "Success", nil
}
func (fcm *MockFcm) SendMultiNotification(title, message string, fcmTokens []string) (string, error) {
	return "Success", nil
}
func (fcm *MockFcm) SendTopicNotification(title, message, topic string) (string, error) {
	return "Success", nil
}
