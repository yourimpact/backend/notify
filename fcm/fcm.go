package fcm

import (
	"context"
	"fmt"

	firebase "firebase.google.com/go"
	"firebase.google.com/go/messaging"
	"gitlab.com/yourimpact/backend/notify/config"
	"google.golang.org/api/option"
)

type Fcm interface {
	SendSingleNotification(title, message, fcmToken string) (string, error)
	SendMultiNotification(title, message string, fcmTokens []string) (string, error)
	SendTopicNotification(title, message, topic string) (string, error)
}

type RealFcm struct {
	client *messaging.Client
}

func New(cfg *config.Config) (*RealFcm, error) {
	opt := option.WithCredentialsFile(cfg.FirebaseCredentialsPath)
	app, err := firebase.NewApp(context.Background(), nil, opt)
	if err != nil {
		return nil, fmt.Errorf("error initializing app: %v", err)
	}
	ctx := context.Background()
	client, err := app.Messaging(ctx)
	if err != nil {
		return nil, fmt.Errorf("error getting Messaging client: %v\n", err)
	}
	return &RealFcm{client: client}, nil
}

func (fcm *RealFcm) SendSingleNotification(title, message, fcmToken string) (string, error) {
	msg := &messaging.Message{
		Notification: &messaging.Notification{
			Title: title,
			Body:  message,
		},
		Token: fcmToken,
	}

	response, err := fcm.client.Send(context.Background(), msg)
	if err != nil {
		return "", err
	}

	return fmt.Sprintln("Successfully sent message:", response), nil
}

func (fcm *RealFcm) SendMultiNotification(title, message string, fcmTokens []string) (string, error) {
	msg := &messaging.MulticastMessage{
		Notification: &messaging.Notification{
			Title: title,
			Body:  message,
		},
		Tokens: fcmTokens,
	}

	response, err := fcm.client.SendMulticast(context.Background(), msg)
	if err != nil {
		return "", err
	}

	return fmt.Sprintln("Successfully sent message:", response), nil
}

func (fcm *RealFcm) SendTopicNotification(title, message, topic string) (string, error) {
	msg := &messaging.Message{
		Notification: &messaging.Notification{
			Title: title,
			Body:  message,
		},
		Topic: topic,
	}

	response, err := fcm.client.Send(context.Background(), msg)
	if err != nil {
		return "", err
	}

	return fmt.Sprintln("Successfully sent message:", response), nil
}
