module gitlab.com/yourimpact/backend/notify

go 1.16

require (
	cloud.google.com/go/firestore v1.5.0 // indirect
	firebase.google.com/go v3.13.0+incompatible
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-playground/universal-translator v0.17.0 // indirect
	github.com/go-playground/validator v9.31.0+incompatible // indirect
	github.com/google/martian v2.1.0+incompatible // indirect
	github.com/google/uuid v1.2.0
	github.com/gorilla/mux v1.8.0
	github.com/jmoiron/sqlx v1.3.4
	github.com/leodido/go-urn v1.2.1 // indirect
	github.com/lestrrat-go/jwx v1.2.1
	github.com/lib/pq v1.10.2
	github.com/rs/cors v1.7.0
	github.com/stretchr/testify v1.7.0
	github.com/swaggo/http-swagger v1.0.0
	github.com/swaggo/swag v1.7.0 // indirect
	github.com/tim-koehler/tims-logger v0.0.0-20210508215237-9a6abb4866eb // indirect
	google.golang.org/api v0.40.0
	gopkg.in/yaml.v2 v2.4.0
)
