# impact  - notify

[![pipeline status](https://gitlab.com/yourimpact/backend/notify/badges/dev/pipeline.svg)](https://gitlab.com/yourimpact/backend/notify/-/commits/dev) [![coverage report](https://gitlab.com/yourimpact/backend/notify/badges/dev/coverage.svg)](https://gitlab.com/yourimpact/backend/notify/-/commits/dev) 

This microservice handles all about notifications. From storing, updating and deleting fcm registration tokens to sending notifications for specific clients, a group of clients or whole topics.

## Setup & Usage
- Initialize database and run migrations using our separate [database repo](https://gitlab.com/yourimpact/backend/database)
- Copy `config.template.yaml`, rename to `config.yaml` and fill out the template according to your desired configuration.
- To build and run: `go build && ./notify`

## OpenAPI / Swagger
- generate with `swag init` after changes werde made. (You need to download that tool: https://github.com/swaggo/swag)
Swagger UI automatically runs under /swagger.

## Tests
For the tests to run you need a local postgres database running and the hostname `postgres` needs to be resolvable to that. This can easily be configured in `/etc/hosts`