package config

import (
	"io/ioutil"

	"gopkg.in/yaml.v2"
)

type Config struct {
	AuthJWKsUri             string `yaml:"AuthJWKsUri"`
	AuthJWKID               string `yaml:"AuthJWKID"`
	AuthBaseUrl             string `yaml:"AuthBaseUrl"`
	AuthApiKey              string `yaml:"AuthApiKey"`
	RouterPrefix            string `yaml:"RouterPrefix"`
	FirebaseCredentialsPath string `yaml:"FirebaseCredentialsPath"`
	LogLevel                string `yaml:"LogLevel"`
	LogType                 string `yaml:"LogType"`
	EnableJwtAuthorization  bool   `yaml:"EnableJwtAuthorization"`
	Database                struct {
		Host     string `yaml:"Host"`
		Port     int    `yaml:"Port"`
		DbName   string `yaml:"DbName"`
		User     string `yaml:"User"`
		Password string `yaml:"Password"`
	} `yaml:"Database"`
	SwaggerHost string `yaml:"SwaggerHost"`
}

func Load(pathToFile string) (*Config, error) {
	data, err := ioutil.ReadFile(pathToFile)
	if err != nil {
		return nil, err
	}
	cfg := Config{}
	if err := yaml.Unmarshal(data, &cfg); err != nil {
		return nil, err
	}

	return &cfg, nil
}
