package config

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestConfig(t *testing.T) {
	cfg, err := Load("../config.template.yaml")
	assert.NoError(t, err)
	assert.NotNil(t, cfg)
	assert.EqualValues(t, "localhost", cfg.SwaggerHost)
	assert.EqualValues(t, "/notify", cfg.RouterPrefix)
}
